import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AbstractControl, ValidationErrors } from '@angular/forms';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


@Injectable({
  providedIn: 'root'
})
export class GenericUtilsService {

  constructor(
    private translate: TranslateService
  ) { }

  public getFormControlErrorMessagesTranslated(control: AbstractControl): string[] {
    const errorsMessages: string[] = [];

    const validationErrors: ValidationErrors = control.errors ? control.errors : [];

    for (let key of Object.keys(validationErrors)) {
      switch (key) {
        case 'required':
          errorsMessages.push(this.translate.instant('GLOBAL.FORM_VALIDATION_ERRORS.REQUIRED'));
          break;
        case 'minlength':
          errorsMessages.push(this.translate.instant('GLOBAL.FORM_VALIDATION_ERRORS.MIN_LENGTH', validationErrors[key]));
          break;
        case 'maxlength':
          errorsMessages.push(this.translate.instant('GLOBAL.FORM_VALIDATION_ERRORS.MAX_LENGTH', validationErrors[key]));
          break;
        case 'email':
          errorsMessages.push(this.translate.instant('GLOBAL.FORM_VALIDATION_ERRORS.EMAIL'));
          break;
        case 'pattern':
          if (validationErrors[key].requiredPattern == RegexExpressions.ONLY_NUMBERS) {
            errorsMessages.push(this.translate.instant('GLOBAL.FORM_VALIDATION_ERRORS.PATTERNS.ONLY_NUMBERS'));
          } else if(validationErrors[key].requiredPattern == RegexExpressions.LETTERS_WITH_SINGLE_SPACE_BETWEEN_WORDS) {
            errorsMessages.push(this.translate.instant('GLOBAL.FORM_VALIDATION_ERRORS.PATTERNS.LETTERS_WITH_SINGLE_SPACE_BETWEEN_WORDS'));
          } else if(validationErrors[key].requiredPattern == RegexExpressions.ONLY_INTEGER_NUMBERS_FOR_DOT_MASK) {
            errorsMessages.push(this.translate.instant('GLOBAL.FORM_VALIDATION_ERRORS.PATTERNS.ONLY_INTEGER_NUMBERS_FOR_DOT_MASK'));
          } else if(validationErrors[key].requiredPattern == RegexExpressions.CO_CAR_PLATE_NUMBER) {
            errorsMessages.push(this.translate.instant('GLOBAL.FORM_VALIDATION_ERRORS.PATTERNS.CO_CAR_PLATE_NUMBER'));
          } else if(validationErrors[key].requiredPattern == RegexExpressions.CO_MOTORCYCLE_PLATE_NUMBER) {
            errorsMessages.push(this.translate.instant('GLOBAL.FORM_VALIDATION_ERRORS.PATTERNS.CO_MOTORCYCLE_PLATE_NUMBER'));
          }
          break;
      }
    }

    return errorsMessages;
  }

  public getDateForService(date: string): string {
    const dateFormatted: Date = new Date(date);
    
    let month = (dateFormatted.getUTCMonth() + 1).toString();
    if (month.length === 1) {
      month = '0' + month;
    }

    let day = dateFormatted.getUTCDate().toString();
    if (day.length === 1) {
      day = '0' + day;
    }

    const dateFormattedString = dateFormatted.getUTCFullYear() + '-' + month + '-' + day;

    return dateFormattedString;
  }
}

export const RegexExpressions = {
  ONLY_NUMBERS: /^[0-9]\d*$/,
  ONLY_INTEGER_NUMBERS_FOR_DOT_MASK: /([0-9]+(\.[0-9]+)?)/,
  LETTERS_WITH_SINGLE_SPACE_BETWEEN_WORDS: /(^(?![\s\S]))|(^([a-zA-Z]+\s)*[a-zA-Z]+$)/, // This will match empty string as well
  CO_CAR_PLATE_NUMBER: /^[A-Z]{3}\d{3}$/,
  CO_MOTORCYCLE_PLATE_NUMBER: /^[A-Z]{3}\d{2}[A-Z]{1}$/,
  ONE_WORD: /^[A-Za-z0-9._-]+$/
}
