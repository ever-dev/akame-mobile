import { FormGroup, ValidatorFn, FormControl } from '@angular/forms';

export const SharedValidators = {
  sameValueAs(group: FormGroup, controlName: string): ValidatorFn {
    return (control: FormControl) => {
          const myValue = control.value;
          const compareValue = group.controls[controlName].value;
  
          return (myValue === compareValue) ? null : {valueDifferentFrom:controlName};
  
    };
  }
}
