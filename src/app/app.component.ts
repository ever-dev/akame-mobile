import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Platform, AlertController, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TranslateService } from '@ngx-translate/core';
import { Router, Event, NavigationEnd } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { AppConfigService } from './services/config.service';
import { AuthState } from './store/auth/auth.state';
import { StateReset } from 'ngxs-reset-plugin';
import { Store, Select } from '@ngxs/store';
import { AuthRefreshToken, AuthSetModel } from './store/auth/auth.actions';
import { AuthStateSelectors } from './store/auth/auth.selectors';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { AppSetSpinnerText } from './store/app/app.actions';
import { AppStateSelectors } from './store/app/app.selectors';


declare var signalR;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  public selectedIndex = 0;
  public appPages: any[];

  public showSpinner: boolean;
  @Select(AppStateSelectors.getSpinner) showSpinner$: Observable<boolean>;
  private showSpinnerSubscription: Subscription;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private translate: TranslateService,
    private router: Router,
    private store: Store,
    public alertController: AlertController,
    private androidPermissions: AndroidPermissions,
    public menuCtrl: MenuController,
    private cdr: ChangeDetectorRef
  ) {
    this.initializeApp();
    this.appPages = [
      {
        title: this.translate.instant('MENU.CALCULATOR'),
        url: '/loan-wizard/calculator',
        icon: 'calculator'
      },
      {
        title: this.translate.instant('MENU.SIGNUP'),
        url: '/signup',
        icon: 'person-add'
      },
      {
        title: this.translate.instant('MENU.LOGIN'),
        url: '/login',
        icon: 'person'
      }
    ];
  }

  private changeItemMenuStateByUrl(url: string, hide: boolean): void {
    this.appPages = this.appPages.map(
      (page: any, index: number, pages: any[]) => {
        if (page.url === url) {
          return {
            ...page,
            hide: hide
          };
        }
        return page;
      }
    )
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      if (this.platform.is('android')) {
        this.androidPermissions.requestPermissions(
          [
            this.androidPermissions.PERMISSION.RECORD_AUDIO,
            this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE,
            this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
          ]
        );
      }
    });
  }

  private routerSubscription: Subscription;


  public token: string;
  @Select(AuthStateSelectors.getToken) token$: Observable<string>;
  private tokenSubscription: Subscription;

  ngOnInit() {
    // This is needed to highlight the selected page in the menu when the route is changed
    this.routerSubscription = this.router.events.subscribe(
      (event: Event) => {
        if (event instanceof NavigationEnd) {
          const pageIndex = this.appPages.findIndex(page => page.url === event.url);
          if (pageIndex !== -1) {
            this.selectedIndex = pageIndex;
          }
        }
      }
    );

    this.tokenSubscription = this.token$.subscribe(
      (token: string) => {
        this.token = token;
        if (this.tokenAlert) {
          this.tokenAlert.dismiss();
        }

        if (token) {
          this.changeItemMenuStateByUrl('/welcome', false);
          this.changeItemMenuStateByUrl('/signup', true);
          this.changeItemMenuStateByUrl('/login', true);
        } else {
          this.changeItemMenuStateByUrl('/welcome', true);
          this.changeItemMenuStateByUrl('/signup', false);
          this.changeItemMenuStateByUrl('/login', false);
        }
      }
    );

    this.showSpinnerSubscription = this.showSpinner$.subscribe(
      ((showSpinner: boolean) => {
        this.showSpinner = showSpinner;
        this.cdr.detectChanges();
      })
    );
  
    this.getTokenOnReload();
    this.validateToken();
    this.configureSpinnerDefaultText();
  }

  private configureSpinnerDefaultText(): void {
    this.store.dispatch(new AppSetSpinnerText(this.translate.instant('GLOBAL.SPINNER_TEXT')));
  }

  private getTokenOnReload() {
    const auth = window.localStorage.getItem('auth');
    if (auth) {
      const authObject = JSON.parse(auth);
      this.store.dispatch(new AuthSetModel(authObject));
    }
  }

  private timeoutId: any;
  public validateToken(): void {
    this.timeoutId = setTimeout(async () => {
      const auth = window.localStorage.getItem('auth');

      const currentTimeStamp = (Math.floor(Date.now() / 1000));
      const storedTimeStamp = window.localStorage.getItem('storedTimeStamp') ? Number(window.localStorage.getItem('storedTimeStamp')) : currentTimeStamp;
      const timeStampDifference = currentTimeStamp - storedTimeStamp;

      if (auth) {
        const authObject = JSON.parse(auth);
        // Update the secondsWhenGenerated value with the time stamp difference
        authObject.secondsWhenGenerated = authObject.secondsWhenGenerated + timeStampDifference;

        if (authObject.token && authObject.secondsWhenGenerated !== 0 && authObject.secondsUntilExpire !== 0) {
          const boundToShowRefreshToken = (authObject.secondsUntilExpire - AppConfigService.settings.appInfo.secondsUntilTokenAlert);

          if (authObject.secondsWhenGenerated >= authObject.secondsUntilExpire) {
            clearTimeout(this.timeoutId);
            this.logout();
            return;
          } else if (authObject.secondsWhenGenerated >= boundToShowRefreshToken) {
            await this.showRefreshTokenAlert(authObject.secondsUntilExpire - authObject.secondsWhenGenerated);
          }

          window.localStorage.setItem('auth', JSON.stringify(authObject));
        }
      }
      window.localStorage.setItem('storedTimeStamp', currentTimeStamp.toString());
      this.validateToken();
    }, 1000);
  }

  private logout(): void {
    this.store.dispatch(new StateReset(AuthState));
    if (this.tokenAlert) {
      this.tokenAlert.dismiss();
      this.tokenAlert = undefined;
    }
    window.localStorage.removeItem('auth');
    this.validateToken();
    this.router.navigateByUrl('/login');
  }

  private tokenAlert: HTMLIonAlertElement = undefined;
  private async showRefreshTokenAlert(secondsRemaining: number) {
    if (!this.tokenAlert) {
      this.tokenAlert = await this.alertController.create({
        cssClass: 'token-modal',
        header: this.translate.instant('TOKEN_MODAL.MODAL_TITLE'),
        message: this.translate.instant('TOKEN_MODAL.MODAL_MESSAGE', { seconds: secondsRemaining }),
        backdropDismiss: false,
        buttons: [
          {
            text: this.translate.instant('TOKEN_MODAL.LOGOUT_BUTTON_LABEL'),
            cssClass: 'token-modal-button',
            handler: () => {
              this.logout();
            }
          },
          {
            text: this.translate.instant('TOKEN_MODAL.REFRESH_SESSION_BUTTON_LABEL'),
            cssClass: 'token-modal-button',
            handler: () => {
              this.store.dispatch(new AuthRefreshToken());
              this.tokenAlert = undefined;
            }
          }
        ]
      });

      await this.tokenAlert.present();
    } else {
      this.tokenAlert.message = this.translate.instant('TOKEN_MODAL.MODAL_MESSAGE', { seconds: secondsRemaining });
    }
  }

  public logoutAction(): void {
    this.logout();
  }

  public showTermConditions(): void {
    this.menuCtrl.close();
    this.router.navigateByUrl('/documents/terms-conditions');
  }

  ngOnDestroy() {
    this.routerSubscription.unsubscribe();
    this.tokenSubscription.unsubscribe();
    this.showSpinnerSubscription.unsubscribe();
    clearTimeout(this.timeoutId);
  }
}
