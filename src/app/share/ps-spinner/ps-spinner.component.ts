import { ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';
import { AppStateSelectors } from 'src/app/store/app/app.selectors';

@Component({
  selector: 'app-shared-component-ps-spinner',
  templateUrl: './ps-spinner.component.html',
  styleUrls: ['./ps-spinner.component.scss']
})
export class PsSpinnerComponent implements OnInit, OnDestroy {
  @Input() borderRadius = '0px';
  @Input() marginTop = '0px';
  @Input() height = '100%';

  public spinnerText = '';
  @Select(AppStateSelectors.getSpinnerText) spinnerText$: Observable<string>;
  private spinnerTextSubscription: Subscription;

  constructor(
    private cdr: ChangeDetectorRef
  ) { }


  ngOnInit(): void {
    this.spinnerTextSubscription = this.spinnerText$.subscribe(
      ((spinnerText: string) => {
        this.spinnerText = spinnerText;
        this.cdr.detectChanges();
      })
    );
  }

  ngOnDestroy(): void {
    this.spinnerTextSubscription.unsubscribe();
  }

}
