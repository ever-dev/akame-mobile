import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { DocumentViewerComponent } from './document-viewer/document-viewer.component';
import { TranslateModule } from '@ngx-translate/core';
import { SafeHtmlPipe } from './safe-html/safe-html.pipe';
import { PsSpinnerComponent } from './ps-spinner/ps-spinner.component';


@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        IonicModule,
        TranslateModule.forChild()
    ],
    exports: [
        DocumentViewerComponent,
        SafeHtmlPipe,
        PsSpinnerComponent
    ],
    declarations: [
        DocumentViewerComponent,
        SafeHtmlPipe,
        PsSpinnerComponent
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
    entryComponents: []
})
export class ShareModule { }
