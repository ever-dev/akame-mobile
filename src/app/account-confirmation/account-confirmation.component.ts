import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { AuthConfirmAccount } from '../store/auth/auth.actions';
import { Observable, Subscription } from 'rxjs';
import { AuthStateSelectors } from '../store/auth/auth.selectors';

@Component({
  selector: 'account-confirmation',
  templateUrl: './account-confirmation.component.html',
  styleUrls: ['./account-confirmation.component.scss'],
})
export class AccountConfirmationComponent implements OnInit,  OnDestroy {

  public token: string;
  public isTokenValid: boolean;
  
  private sub: Subscription;
  private isTokenValidSubscription: Subscription;

  @Select(AuthStateSelectors.getIsValidToken) isTokenValid$: Observable<boolean>;
  
  constructor(
    private store: Store,
    private location: Location,
    public router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.isTokenValid = false;

    this.isTokenValidSubscription = this.isTokenValid$.subscribe(
      ((isTokenValid: boolean) => {
        this.isTokenValid = isTokenValid;
      })
    );

    this.sub = this.route.params.subscribe(params => {
      this.token = params['token'];

      this.store.dispatch(new AuthConfirmAccount(this.token));
   });
  }

  ngOnDestroy() {
  }

  public backButtonAction(): void {
    this.location.back();
  }

  public goLogin(): void {
    this.router.navigateByUrl('/login');
  }

  public goRegister(): void {
    this.router.navigateByUrl('/signup');
  }    
}
