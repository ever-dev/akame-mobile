import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { AccountConfirmationRoutingModule } from './account-confirmation-routing.module';
import { AccountConfirmationComponent } from './account-confirmation.component';
import { ShareModule } from '../share/share.module';
import { DocumentService } from '../services/document.service';
import { NgxsModule } from '@ngxs/store';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        AccountConfirmationRoutingModule,
        NgxsModule.forFeature([]),
        TranslateModule.forChild(),
        ReactiveFormsModule,
        ShareModule
    ],
    declarations: [
        AccountConfirmationComponent
    ],
    providers: [
        DocumentService
    ]
})
export class AccountConfirmationModule { }
