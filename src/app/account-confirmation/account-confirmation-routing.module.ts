import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountConfirmationComponent } from './account-confirmation.component';


const routes: Routes = [
    {
        path: '',
        component: AccountConfirmationComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AccountConfirmationRoutingModule { }
