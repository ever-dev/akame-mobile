import { Component, OnInit, OnDestroy } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { AuthGetToken, AuthSetModel } from '../store/auth/auth.actions';
import { Observable, Subscription } from 'rxjs';
import { AuthStateSelectors } from '../store/auth/auth.selectors';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  @Select(AuthStateSelectors.getUsername) username$: Observable<string>;
  private usernameSubscription: Subscription;

  @Select(AuthStateSelectors.getPassword) password$: Observable<string>;
  private passwordSubscription: Subscription;

  public loginForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  constructor(
    private store: Store,
    private location: Location,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.usernameSubscription = this.username$.subscribe(
      (username: string) => {
        this.loginForm.get('name').setValue(username);
        this.loginForm.markAsUntouched();
      }
    );

    this.passwordSubscription = this.password$.subscribe(
      (password: string) => {
        this.loginForm.get('password').setValue(password);
        this.loginForm.markAsUntouched();
      }
    );

    // Get Query Parameters
    this.route.queryParams.subscribe(params => {
      if (params['tfc'] === 'BacSpeXKHg9Mhyv4ksZnrQwBn2YcYgwvJDZ99kU5eCgBGn6BDH') { // Text For Credit
        window.localStorage.setItem('tfc', 'BacSpeXKHg9Mhyv4ksZnrQwBn2YcYgwvJDZ99kU5eCgBGn6BDH'); // Store in the localStorage
        this.router.navigateByUrl('/login');
      }
    });
  }

  public backButtonAction(): void {
    this.location.back();
  }

  public onSubmit(): void {
    this.store.dispatch(new AuthSetModel(this.loginForm.value));
    this.store.dispatch(new AuthGetToken());
  }

  ngOnDestroy(): void {
    this.usernameSubscription.unsubscribe();
    this.passwordSubscription.unsubscribe();
  }
}
