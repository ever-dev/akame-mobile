import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ShareModule } from './share/share.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory, GenericUtilsService } from 'src/shared/utils';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Globalization } from '@ionic-native/globalization/ngx';
import { NgxsModule } from '@ngxs/store';
import { environment } from 'src/environments/environment';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { NgxsResetPluginModule } from 'ngxs-reset-plugin';
import { AppConfigService } from './services/config.service';
import { ServiceUtils } from './services/utils';
import { AuthGuard } from './services/auth.guard';
import { NgxMaskModule } from 'ngx-mask';
import { AuthState } from './store/auth/auth.state';
import { AuthService } from './services/auth.service';
import { HelpService } from './services/help.service';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { AppState } from './store/app/app.state';


export function initializeApp(appConfigService: AppConfigService) {
  return (): Promise<any> => {
    return Promise.all([appConfigService.load(), appConfigService.loadTranslations()]);
  }
}

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    }),
    ShareModule,
    AppRoutingModule,
    NgxsModule.forRoot([AppState, AuthState], { developmentMode: !environment.production }),
    NgxsStoragePluginModule.forRoot({key: []}),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    NgxsResetPluginModule.forRoot(),
    NgxMaskModule.forRoot()
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Globalization,
    AppConfigService,
    { provide: APP_INITIALIZER, useFactory: initializeApp, deps: [AppConfigService], multi: true},
    ServiceUtils,
    AuthGuard,
    GenericUtilsService,
    AuthService,
    HelpService,
    AndroidPermissions,
    InAppBrowser 
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
