import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './services/auth.guard';


const routes: Routes = [
  {
    path: '',
    redirectTo: '/loan-wizard/calculator',
    pathMatch: 'full'
  }, 
  {
    path: 'account-confirmation/:token',
    loadChildren: () => import('./account-confirmation/account-confirmation.module').then( m => m.AccountConfirmationModule)
  },
  {
    path: 'loan-wizard',
    loadChildren: () => import('./loan-wizard/loan-wizard.module').then( m => m.LoanWizardModule)
  },
  {
    path: 'login',
    canActivate: [AuthGuard],
    loadChildren: () => import('./login/login.module').then( m => m.LoginModule)
  },
  {
    path: 'signup',
    canActivate: [AuthGuard],
    loadChildren: () => import('./signup/signup.module').then( m => m.SignupModule)
  },
  {
    path: 'have-account',
    canActivate: [AuthGuard],
    loadChildren: () => import('./have-account/have-account.module').then( m => m.HaveAccountModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./forgot-password/forgot-password.module').then( m => m.ForgotPasswordModule)
  },
  {
    path: 'forgot-password/:token',
    loadChildren: () => import('./forgot-password/forgot-password.module').then( m => m.ForgotPasswordModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})

export class AppRoutingModule {}
