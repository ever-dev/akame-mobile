import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngxs/store';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { AuthSetModel, AuthCallSignup } from '../store/auth/auth.actions';
import { SharedValidators } from 'src/shared/validators';
import { RegexExpressions } from 'src/shared/utils';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit, OnDestroy {

  public signupForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.pattern(RegexExpressions.ONE_WORD), Validators.minLength(4), Validators.maxLength(10)]),
    email: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
    confirmPassword: new FormControl('', [Validators.required])
  });

  constructor(
    private store: Store,
    private location: Location
  ) {
  }

  ngOnInit(): void {
    this.signupForm.get('confirmPassword').setValidators([Validators.required, SharedValidators.sameValueAs(this.signupForm, 'password')]);
  }

  public backButtonAction(): void {
    this.location.back();
  }

  public onSubmit(): void {
    this.store.dispatch(new AuthSetModel(this.signupForm.value));
    this.store.dispatch(new AuthCallSignup());
  }

  ngOnDestroy(): void {
  }
}
