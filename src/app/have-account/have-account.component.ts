import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngxs/store';
import { Location } from '@angular/common';
import { Router } from '@angular/router';


@Component({
  selector: 'app-have-account',
  templateUrl: './have-account.component.html',
  styleUrls: ['./have-account.component.scss'],
})
export class HaveAccountComponent implements OnInit, OnDestroy {

  constructor(
    private store: Store,
    private location: Location,
    private router: Router
  ) {
  }

  ngOnInit(): void {
  }

  public backButtonAction(): void {
    this.location.back();
  }

  public loginButtonAction(): void {
    this.router.navigateByUrl('/login');
  }

  public signupButtonAction(): void {
    this.router.navigateByUrl('/signup');
  }

  ngOnDestroy(): void {
  }
}
