import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { HaveAccountRoutingModule } from './have-account-routing.module';
import { HaveAccountComponent } from './have-account.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        HaveAccountRoutingModule,
        TranslateModule.forChild(),
        ReactiveFormsModule
    ],
    declarations: [
        HaveAccountComponent
    ]
})
export class HaveAccountModule { }
