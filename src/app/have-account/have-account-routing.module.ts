import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HaveAccountComponent } from './have-account.component';


const routes: Routes = [
    {
        path: '',
        component: HaveAccountComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class HaveAccountRoutingModule { }
