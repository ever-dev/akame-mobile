import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngxs/store';
import { FormGroup, FormControl, Validators, ValidationErrors, AbstractControl } from '@angular/forms';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { AuthChangePassword, AuthSendForgotPasswordEmail } from '../store/auth/auth.actions';


@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent implements OnInit, OnDestroy {

  public token: string;

  private matchValues(
    matchTo: string
  ): (AbstractControl) => ValidationErrors | null {
    return (control: AbstractControl): ValidationErrors | null => {
      return !!control.parent &&
        !!control.parent.value &&
        control.value === control.parent.controls[matchTo].value
        ? null
        : { isMatching: false };
    };
  }

  public sendEmailForm = new FormGroup({
    nameOrEmail: new FormControl('', [Validators.required])
  });

  public changePasswordForm = new FormGroup({
    password: new FormControl('', [Validators.required]),
    passwordConfirmation: new FormControl('', [Validators.required, this.matchValues('password')])
  });

  constructor(
    private store: Store,
    private location: Location,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.token = this.route.snapshot.paramMap.get('token');
  }

  public backButtonAction(): void {
    this.location.back();
  }

  public onSendEmailFormSubmit(): void {
    this.store.dispatch(new AuthSendForgotPasswordEmail(this.sendEmailForm.get('nameOrEmail').value));
  }

  public onChangePasswordFormSubmit(): void {
    this.store.dispatch(new AuthChangePassword({ token: this.token, password: this.changePasswordForm.get('password').value }));
  }

  ngOnDestroy(): void {
  }
}
