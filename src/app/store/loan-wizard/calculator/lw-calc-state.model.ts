export interface LoanWizardCalculatorConfigurationTerm {
    termInDays: number;
    paymentDays: number[];
}

export interface LoanWizardCalculatorConfiguration {
    minAmount: number;
    maxAmount: number;
    stepAmount: number;
    minDays: number;
    maxDays: number;
    termConfiguration: LoanWizardCalculatorConfigurationTerm[];
}

export interface LoanWizardCalculatorDetails {
    loanAmount: number;
    totalInterests: number;
    insurance: number;
    variableDigitalServices: number;
    fixedDigitalServices: number;
    disbursement: number;
    creditValidation: number;
    promissoryNoteAndCustody: number;
    endorsement: number;
    collection: number;
    vat: number;
    administration: number;
    totalDigitalServices: number;
    total: number;
    paymentAmount: number;
}

export interface LoanWizardCalculatorModelState {
    amount: number;
    paymentsQuantity: number;
    expirationDate: Date;
    termInDays?: number;
    configuration?: LoanWizardCalculatorConfiguration;
    details?: LoanWizardCalculatorDetails;
    spinnerState?: boolean;
    supplierId?: number;    
}
