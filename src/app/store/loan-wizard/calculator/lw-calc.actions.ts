import { LoanWizardCalculatorModelState } from './lw-calc-state.model';

export class LoanWizardCalculatorSetModel {
  static type = '[Loan Wizard Calculator] Set Model';
  constructor(public payload: LoanWizardCalculatorModelState) { }
}

export class LoanWizardCalculatorGetConfiguration {
  static type = '[Loan Wizard Calculator] Get Configuration';
  constructor(public supplierId: number) { }
}

export class LoanWizardCalculatorGetDetails {
  static type = '[Loan Wizard Calculator] Get Details';
  constructor() { }
}

export class LoanWizardCalculatorSaveLoanConfiguration {
  static type = '[Loan Wizard Calculator] Save Loan Configuration';
  constructor() { }
}

export class LoanWizardCalculatorSetSpinnerState {
  static type = '[Loan Wizard Calculator] Set Spinner State';
  constructor(public payload: boolean) { }
}
