import { Selector } from '@ngxs/store';
import { LoanWizardCalculatorState } from './lw-calc.state';
import { LoanWizardCalculatorModelState } from './lw-calc-state.model';

export class LoanWizardCalculatorStateSelectors {

  @Selector([LoanWizardCalculatorState])
  static getAmount(state: LoanWizardCalculatorModelState) {
    return state.amount;
  }

  @Selector([LoanWizardCalculatorState])
  static getPaymentsQuantity(state: LoanWizardCalculatorModelState) {
    return state.paymentsQuantity;
  }

  @Selector([LoanWizardCalculatorState])
  static getExpirationDate(state: LoanWizardCalculatorModelState) {
    return state.expirationDate;
  }
  
  @Selector([LoanWizardCalculatorState])
  static getConfiguration(state: LoanWizardCalculatorModelState) {
    return state.configuration;
  }

  @Selector([LoanWizardCalculatorState])
  static getDetails(state: LoanWizardCalculatorModelState) {
    return state.details;
  }

  @Selector([LoanWizardCalculatorState])
  static getSpinnerState(state: LoanWizardCalculatorModelState) {
    return state.spinnerState;
  }
}
