import { State, Action, StateContext, Store } from '@ngxs/store';
import { tap } from 'rxjs/operators';
import { LoanWizardCalculatorModelState } from './lw-calc-state.model';
import { LoanWizardCalculatorSetModel, LoanWizardCalculatorSetSpinnerState, LoanWizardCalculatorGetConfiguration, LoanWizardCalculatorGetDetails, LoanWizardCalculatorSaveLoanConfiguration } from './lw-calc.actions';
import { ToastController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { ServiceResponse } from 'src/app/services/service-response';
import { CalculatorService } from 'src/app/services/calculator.service';
import { TranslateService } from '@ngx-translate/core';
import { ServiceUtils } from 'src/app/services/utils';
import { CustomerService } from 'src/app/services/customer.service';
import { AuthStateSelectors } from '../../auth/auth.selectors';
import { Router } from '@angular/router';
import { StateReset } from 'ngxs-reset-plugin';
import { AppSetSpinner } from '../../app/app.actions';

@State<LoanWizardCalculatorModelState>({
  name: 'loanWizardCalculator',
  defaults: {
    amount: 200,
    paymentsQuantity: 1,
    expirationDate: new Date(),
    termInDays: undefined,
    configuration: undefined,
    details: undefined,
    spinnerState: false
  }
})
export class LoanWizardCalculatorState {
  constructor(
    private store: Store,
    public toastController: ToastController,
    private calculatorService: CalculatorService,
    private translate: TranslateService,
    private serviceUtils: ServiceUtils,
    private customerService: CustomerService,
    public router: Router
  ) { }

  @Action(LoanWizardCalculatorSetModel)
  setModel({ patchState, getState }: StateContext<LoanWizardCalculatorModelState>, { payload }: LoanWizardCalculatorSetModel) {
    patchState({
      ...payload
    });
  }

  @Action(LoanWizardCalculatorGetConfiguration)
  getConfiguration({ patchState, getState }: StateContext<LoanWizardCalculatorModelState>, { supplierId }: LoanWizardCalculatorGetConfiguration) {
    const state = getState();    
    this.store.dispatch(new AppSetSpinner(true));

    return this.calculatorService.getConfiguration(
      {
      supplierId: supplierId
      }
    ).pipe(
      tap(async (content: ServiceResponse<any>) => {
        ;
        if (!content.details.isError) {
          patchState({
            configuration: content.data
          });
        }
        else {
          const toast = await this.toastController.create({
            message: this.translate.instant('CALCULATOR.ERROR_GET_CONFIGURATION'),
            position: 'bottom',
            duration: 5000
          });
          toast.present();

          if (!environment.production && content.details.exception) {
            console.error(content.details.exception);
          }
        }
        this.store.dispatch(new AppSetSpinner(false));
      }, async (error: any) => {
        await this.serviceUtils.genericServiceErrorToast();
        if (!environment.production) {
          console.error(error);
        }

        this.store.dispatch(new AppSetSpinner(false));
      })
    );
  }

  @Action(LoanWizardCalculatorGetDetails)
  getDetails({ patchState, getState }: StateContext<LoanWizardCalculatorModelState>, { }: LoanWizardCalculatorGetDetails) {
    const state = getState();
    this.store.dispatch(new AppSetSpinner(true));

    return this.calculatorService.getDetails(
      {
        amount: state.amount,
        paymentsQuantity: state.paymentsQuantity,
        termInDays: state.termInDays
      }
    ).pipe(
      tap(async (content: ServiceResponse<any>) => {
        ;
        if (!content.details.isError) {
          patchState({
            details: content.data
          });
        }
        else {
          const toast = await this.toastController.create({
            message: this.translate.instant('CALCULATOR.ERROR_GET_DETAILS'),
            position: 'bottom',
            duration: 5000
          });
          toast.present();

          if (!environment.production && content.details.exception) {
            console.error(content.details.exception);
          }
        }
        this.store.dispatch(new AppSetSpinner(false));
      }, async (error: any) => {
        await this.serviceUtils.genericServiceErrorToast();
        if (!environment.production) {
          console.error(error);
        }

        this.store.dispatch(new AppSetSpinner(false));
      })
    );
  }

  @Action(LoanWizardCalculatorSaveLoanConfiguration)
  saveLoanConfiguration({ patchState, getState }: StateContext<LoanWizardCalculatorModelState>, { }: LoanWizardCalculatorSaveLoanConfiguration) {
    const state = getState();
    this.store.dispatch(new AppSetSpinner(true));

    const token = this.store.selectSnapshot(AuthStateSelectors.getToken);

    return this.customerService.saveLoanConfiguration(token, {
      loan: {
        amount: state.amount,
        paymentsQuantity: state.paymentsQuantity,
        termInDays: state.termInDays
      }
    }
    ).pipe(
      tap(async (content: ServiceResponse<any>) => {
        ;
        if (!content.details.isError) {
          this.router.navigateByUrl('/documents/data-authorization');
        }
        else {
          const toast = await this.toastController.create({
            message: this.translate.instant('CALCULATOR.ERROR_SAVE_LOAN_CONFIGURATION'),
            position: 'bottom',
            duration: 5000
          });
          toast.present();

          if (!environment.production && content.details.exception) {
            console.error(content.details.exception);
          }
        }
        this.store.dispatch(new AppSetSpinner(false));
      }, async (error: any) => {
        await this.serviceUtils.genericServiceErrorToast();
        if (!environment.production) {
          console.error(error);
        }

        this.store.dispatch(new AppSetSpinner(false));
      })
    );
  }

  @Action(LoanWizardCalculatorSetSpinnerState)
  setSpinnerState({ patchState, getState }: StateContext<LoanWizardCalculatorModelState>, { payload }: LoanWizardCalculatorSetSpinnerState) {
    patchState({
      spinnerState: payload
    });
  }
}
