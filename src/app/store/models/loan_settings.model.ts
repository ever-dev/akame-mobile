export interface LoanSettingsModel {
  amount: number;
  paymentsQuantity: number;
  termInDays: number;
}

export interface LoanModel {
  loansStatusId: number;
  statusName: string;
  startDate: Date;
  startDateFormat: string;
  endDate: Date;
  endDateFormat: string;
  disbursementDate: Date;
  disbursementDateFormat: string;
  approvedDate: Date;
  approvedDateFormat: string;
  amountPerQuote: number;
  requestAmount: number;
  requestAmountFormat: string
  interestTotal: number;
  capitalAmount: number;
  totalAmount: number;
  totalAmountFormat: string;
  personsId: number;
  loansConfigurationId: number;
  interestPerQuote: number;
  interestAmountPerQuote: number;
  interestAmountTotal: number;
  isActive: true;
  isDeleted: false;
  lastModifiedBy: number;
  lastModifiedDate: Date;
  lastModifiedByName: string;
  financialLoansId: number
}
