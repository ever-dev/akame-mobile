
export class AppSetSpinner {
  static type = '[App] Set Spinner';
  constructor(public spinner: boolean) { }
}

export class AppSetSpinnerText {
  static type = '[App] Set Spinner Text';
  constructor(public spinnerText: string) { }
}
