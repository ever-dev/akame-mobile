import { State, Action, StateContext } from '@ngxs/store';
import { AppModelState } from './app-state.model';
import { AppSetSpinner, AppSetSpinnerText } from './app.actions';

@State<AppModelState>({
  name: 'app',
  defaults: {
    spinner: false,
    spinnerText: 'Por favor, espere...'
  }
})
export class AppState {
  constructor() { }

  @Action(AppSetSpinner)
  setSpinnerState({ patchState }: StateContext<AppModelState>, { spinner }: AppSetSpinner) {
    patchState({
      spinner: spinner
    });
  }

  @Action(AppSetSpinnerText)
  setSpinnerText({ patchState }: StateContext<AppModelState>, { spinnerText }: AppSetSpinnerText) {
    patchState({
      spinnerText: spinnerText
    });
  }
}
