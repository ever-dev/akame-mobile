import { Selector } from '@ngxs/store';
import { AppModelState } from './app-state.model';
import { AppState } from './app.state';



export class AppStateSelectors {
  @Selector([AppState])
  static getSpinner(state: AppModelState) {
    return state.spinner;
  }

  @Selector([AppState])
  static getSpinnerText(state: AppModelState) {
    return state.spinnerText;
  }
}
