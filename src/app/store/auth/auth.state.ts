import { State, Action, StateContext, Store } from '@ngxs/store';
import { tap } from 'rxjs/operators';
import { ModalController, ToastController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { ServiceResponse } from 'src/app/services/service-response';
import { TranslateService } from '@ngx-translate/core';
import { ServiceUtils } from 'src/app/services/utils';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { AuthModelState } from './auth-state.model';
import { AuthSetModel, AuthGetToken, AuthCallSignup, AuthRefreshToken, AuthConfirmAccount, AuthSendForgotPasswordEmail, AuthChangePassword } from './auth.actions';
import { AppSetSpinner } from '../app/app.actions';

@State<AuthModelState>({
  name: 'auth',
  defaults: {
    name: undefined,
    password: undefined,
    token: undefined,
    secondsWhenGenerated: 0,
    secondsUntilExpire: 0,
    email: undefined,
    permissions: undefined,
    isTokenValid: undefined
  }
})
export class AuthState {
  constructor(
    public toastController: ToastController,
    private authService: AuthService,
    private translate: TranslateService,
    private serviceUtils: ServiceUtils,
    private router: Router,
    private store: Store,
    public modalController: ModalController,
  ) { }

  @Action(AuthSetModel)
  setModel({ patchState }: StateContext<AuthModelState>, { payload }: AuthSetModel) {
    patchState({
      ...payload
    });
  }

  @Action(AuthGetToken)
  getToken({ patchState, getState }: StateContext<AuthModelState>, { }: AuthGetToken) {
    this.store.dispatch(new AppSetSpinner(true));

    const state = getState();

    const tfcFlag = localStorage.getItem('tfc') ? true : false;

    return this.authService.getToken({ auth: { name: state.name, password: state.password, tfc: tfcFlag} }).pipe(
      tap(async (content: ServiceResponse<any>) => {
        if (!content.details.isError) {
          patchState({
            token: content.data.token,
            permissions: content.data.permissions,
            secondsWhenGenerated: content.data.secondsWhenGenerated,
            secondsUntilExpire: content.data.secondsUntilExpire
          });
          localStorage.setItem('auth', JSON.stringify(
            {
              token: content.data.token,
              permissions: content.data.permissions,
              secondsWhenGenerated: content.data.secondsWhenGenerated,
              secondsUntilExpire: content.data.secondsUntilExpire
            }
          ));
          localStorage.removeItem('tfc');
          if (content.data.needRetrySignProcess) {
            await this.protecDataStartModalAction();
          }
          this.router.navigateByUrl('/loan-wizard/calculator');
        }
        else {
          let message_text = this.translate.instant('LOGIN.ERROR_GET_TOKEN');

          if (content.details.codeName === 'INVALID_CREDENTIALS') {
            message_text = this.translate.instant('LOGIN.ERROR_BAD_CREDENTIALS');
          }

          const toast = await this.toastController.create({
            message: message_text,
            position: 'bottom',
            duration: 5000
          });
          toast.present();

          if (!environment.production && content.details.exception) {
            console.error(content.details.exception);
          }
        }
        await this.store.dispatch(new AppSetSpinner(false));
      }, async (error: any) => {
        await this.serviceUtils.genericServiceErrorToast();
        if (!environment.production) {
          console.error(error);
        }

        this.store.dispatch(new AppSetSpinner(false));
      })
    );
  }

  @Action(AuthRefreshToken)
  refreshToken({ patchState, getState }: StateContext<AuthModelState>, { }: AuthRefreshToken) {
    this.store.dispatch(new AppSetSpinner(true));

    const state = getState();

    return this.authService.refreshToken(state.token).pipe(
      tap(async (content: ServiceResponse<any>) => {
        if (!content.details.isError) {
          patchState({
            ...state,
            token: content.data.token,
            secondsWhenGenerated: content.data.secondsWhenGenerated,
            secondsUntilExpire: content.data.secondsUntilExpire
          });
          localStorage.setItem('auth', JSON.stringify(
            {
              ...JSON.parse(localStorage.getItem('auth')),
              token: content.data.token,
              secondsWhenGenerated: content.data.secondsWhenGenerated,
              secondsUntilExpire: content.data.secondsUntilExpire
            }
          ));
        }
        else {
          const toast = await this.toastController.create({
            message: this.translate.instant('GLOBAL.REFRESH_TOKEN_ERROR'),
            position: 'bottom',
            duration: 5000
          });
          toast.present();

          if (!environment.production && content.details.exception) {
            console.error(content.details.exception);
          }
        }
        await this.store.dispatch(new AppSetSpinner(false));
      }, async (error: any) => {
        await this.serviceUtils.genericServiceErrorToast();
        if (!environment.production) {
          console.error(error);
        }

        this.store.dispatch(new AppSetSpinner(false));
      })
    );
  }

  @Action(AuthCallSignup)
  callSignup({ patchState, getState }: StateContext<AuthModelState>, { }: AuthCallSignup) {
    this.store.dispatch(new AppSetSpinner(true));

    const state = getState();

    return this.authService.signup({ user: { name: state.name, password: state.password, email: state.email } }).pipe(
      tap(async (content: ServiceResponse<any>) => {
        if (!content.details.isError) {
          const toast = await this.toastController.create({
            message: this.translate.instant('SIGNUP.ACCOUNT_VERIFICATION_TOAST_MESSAGE'),
            position: 'bottom',
            duration: 8000
          });
          toast.present();
          this.router.navigateByUrl('/login');
        }
        else {
          let message_text = this.translate.instant('SIGNUP.ERROR_SIGNUP');

          if (content.details.codeName === 'ALREADY_EXISTS') {
            message_text = this.translate.instant('SIGNUP.ALREADY_EXISTS');
          }

          const toast = await this.toastController.create({
            message: message_text,
            position: 'bottom',
            duration: 5000
          });
          toast.present();

          if (!environment.production && content.details.exception) {
            console.error(content.details.exception);
          }
        }
        await this.store.dispatch(new AppSetSpinner(false));
      }, async (error: any) => {
        await this.serviceUtils.genericServiceErrorToast();
        if (!environment.production) {
          console.error(error);
        }

        this.store.dispatch(new AppSetSpinner(false));
      })
    );
  }

  @Action(AuthConfirmAccount)
  confirmAccount({ patchState, getState }: StateContext<AuthModelState>, { token }: AuthConfirmAccount) {
    this.store.dispatch(new AppSetSpinner(true));

    const state = getState();

    return this.authService.confirmAccount({ token }).pipe(
      tap(async (content: ServiceResponse<any>) => {
        if (!content.details.isError) {
          patchState({
            isTokenValid: true
          });
        }
        else {
          patchState({
            isTokenValid: false
          });

          if (!environment.production && content.details.exception) {
            console.error(content.details.exception);
          }
        }
        await this.store.dispatch(new AppSetSpinner(false));
      }, async (error: any) => {
        patchState({
          isTokenValid: false
        });
        if (!environment.production) {
          console.error(error);
        }

        this.store.dispatch(new AppSetSpinner(false));
      })
    );
  }


  @Action(AuthSendForgotPasswordEmail)
  sendForgotEmailPassword({ }: StateContext<AuthModelState>, { usernameOrEmail }: AuthSendForgotPasswordEmail) {
    this.store.dispatch(new AppSetSpinner(true));

    return this.authService.sendForgotPasswordEmail({ usernameOrEmail: usernameOrEmail}).pipe(
      tap(async (content: ServiceResponse<any>) => {
        if (!content.details.isError) {
          const toast = await this.toastController.create({
            message: this.translate.instant('FORGOT_PASSWORD.CHECK_YOUR_EMAIL'),
            position: 'bottom',
            duration: 10000
          });
          toast.present();
          this.router.navigateByUrl('/loan-wizard/calculator');
        }
        else {
          let message_text = this.translate.instant('FORGOT_PASSWORD.UNEXPECTED_ERROR');

          if (content.details.codeName === 'OBTAINED_UNSUCCESSFULLY') {
            message_text = this.translate.instant('FORGOT_PASSWORD.USER_OR_EMAIL_NOT_FOUND');
          }

          const toast = await this.toastController.create({
            message: message_text,
            position: 'bottom',
            duration: 8000
          });
          toast.present();

          if (!environment.production && content.details.exception) {
            console.error(content.details.exception);
          }
        }
        await this.store.dispatch(new AppSetSpinner(false));
      }, async (error: any) => {
        await this.serviceUtils.genericServiceErrorToast();
        if (!environment.production) {
          console.error(error);
        }

        this.store.dispatch(new AppSetSpinner(false));
      })
    );
  }

  @Action(AuthChangePassword)
  changePassword({ }: StateContext<AuthModelState>, { payload }: AuthChangePassword) {
    this.store.dispatch(new AppSetSpinner(true));

    return this.authService.changePassword(payload.token, {password: payload.password}).pipe(
      tap(async (content: ServiceResponse<any>) => {
        if (!content.details.isError) {
          const toast = await this.toastController.create({
            message: this.translate.instant('FORGOT_PASSWORD.PASSWORD_CHANGED'),
            position: 'bottom',
            duration: 10000
          });
          toast.present();
          this.router.navigateByUrl('/login');
        }
        else {
          let message_text = this.translate.instant('FORGOT_PASSWORD.UNEXPECTED_ERROR');

          const toast = await this.toastController.create({
            message: message_text,
            position: 'bottom',
            duration: 8000
          });
          toast.present();

          if (!environment.production && content.details.exception) {
            console.error(content.details.exception);
          }
        }
        await this.store.dispatch(new AppSetSpinner(false));
      }, async (error: any) => {
        if(error.status === 401) {
          const toast = await this.toastController.create({
            message: this.translate.instant('FORGOT_PASSWORD.TOKEN_ERROR'),
            position: 'bottom',
            duration: 12000
          });
          toast.present();
        } else {
          await this.serviceUtils.genericServiceErrorToast();
          if (!environment.production) {
            console.error(error);
          }
        }

        this.store.dispatch(new AppSetSpinner(false));
      })
    );
  }

  private async protecDataStartModalAction() { }
}
