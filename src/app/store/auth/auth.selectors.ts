import { Selector } from '@ngxs/store';
import { AuthModelState } from './auth-state.model';
import { AuthState } from './auth.state';


export class AuthStateSelectors {
  @Selector([AuthState])
  static getToken(state: AuthModelState) {
    return state.token;
  }

  @Selector([AuthState])
  static getUsername(state: AuthModelState) {
    return state.name;
  }

  @Selector([AuthState])
  static getPassword(state: AuthModelState) {
    return state.password;
  }

  @Selector([AuthState])
  static getIsValidToken(state: AuthModelState) {
    return state.isTokenValid;
  }
}
