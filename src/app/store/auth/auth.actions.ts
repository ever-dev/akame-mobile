import { AuthModelState } from './auth-state.model';

export class AuthSetModel {
  static type = '[Auth] Set Credentials';
  constructor(public payload: AuthModelState) { }
}

export class AuthGetToken {
  static type = '[Auth] Get Token';
  constructor() { }
}

export class AuthRefreshToken {
  static type = '[Auth] Refresh Token';
  constructor() { }
}

export class AuthCallSignup {
  static type = '[Auth] Call Signup';
  constructor() { }
}

export class AuthConfirmAccount {
  static type = '[Auth] Confirm Account';
  constructor(public token: string) { }
}

export class AuthSetIsValidTokenState {
  static type = '[Auth] Set Is Valid Token';
  constructor(public payload: boolean) { }
}

export class AuthSendForgotPasswordEmail {
  static type = '[Auth] Send Forgot Password Email';
  constructor(public usernameOrEmail: string) { }
}

export class AuthChangePassword {
  static type = '[Auth] Change Password';
  constructor(public payload: {password: string; token: string}) { }
}
