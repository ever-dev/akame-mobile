export interface AuthModelState {
    name: string;
    password: string;
    token: string;
    email: string;
    permissions: any;
    secondsWhenGenerated: number;
    secondsUntilExpire: number;
    isTokenValid: boolean;
}
