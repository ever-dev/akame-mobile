import { NgModule } from '@angular/core';
import { LoanWizardRoutingModule } from './loan-wizard-routing.module';
import { NgxsModule } from '@ngxs/store';
import { CustomerService } from '../services/customer.service';

@NgModule({
    imports: [
        LoanWizardRoutingModule,
        NgxsModule.forFeature([]),
    ],
    providers: [
      CustomerService
  ]
})
export class LoanWizardModule { }
