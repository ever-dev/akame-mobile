import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { LoanWizardCalculatorGetConfiguration, LoanWizardCalculatorGetDetails, LoanWizardCalculatorSetModel, LoanWizardCalculatorSaveLoanConfiguration } from 'src/app/store/loan-wizard/calculator/lw-calc.actions';
import { Observable, Subscription, Subject } from 'rxjs';
import { LoanWizardCalculatorConfiguration, LoanWizardCalculatorDetails } from 'src/app/store/loan-wizard/calculator/lw-calc-state.model';
import { LoanWizardCalculatorStateSelectors } from 'src/app/store/loan-wizard/calculator/lw-calc.selectors';
import { IonRange, IonDatetime, ModalController, ToastController } from '@ionic/angular';
import { ShowDetailsModalComponent } from './show-details-modal/show-details-modal.component';
import { debounceTime, tap } from 'rxjs/operators';
import { Router, ActivatedRoute  } from '@angular/router';
import { AuthStateSelectors } from 'src/app/store/auth/auth.selectors';
import { ServiceUtils } from 'src/app/services/utils';
import { AppConfigService } from 'src/app/services/config.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-loan-wizard-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss'],
})
export class CalculatorComponent implements OnInit, OnDestroy {

  public paymentsQuantity: number;
  public supplierId: number;
  @Select(LoanWizardCalculatorStateSelectors.getPaymentsQuantity) paymentsQuantity$: Observable<number>;
  private paymentsQuantitySubscription: Subscription;


  public configuration: LoanWizardCalculatorConfiguration;
  @Select(LoanWizardCalculatorStateSelectors.getConfiguration) configuration$: Observable<LoanWizardCalculatorConfiguration>;
  private configurationSubscription: Subscription;

  public details: LoanWizardCalculatorDetails;
  @Select(LoanWizardCalculatorStateSelectors.getDetails) details$: Observable<LoanWizardCalculatorDetails>;
  private detailsSubscription: Subscription;


  public invalidDate: boolean = false;
  public daysQuantity: number = 0;
  public minDateString: string  = '';
  public maxDateString: string = '';
  @ViewChild('rangeAmount', { static: true }) rangeAmount: IonRange;
  @ViewChild('dateTimePicker', {static: true}) dateTimePicker: ElementRef<HTMLInputElement>;

  private getDetailsSubject = new Subject<void>();
  private getDetailsSubjectSubscription: Subscription;

  constructor(
    private store: Store
    , public modalController: ModalController
    , public router: Router
    , private route: ActivatedRoute
    , private utils: ServiceUtils
    , public toastController: ToastController
    , private translate: TranslateService
  ) {
  }

  ngOnInit(): void {
    this.configurationSubscription = this.configuration$.subscribe(
      ((config: LoanWizardCalculatorConfiguration) => {
        this.configuration = config;
        let supplierId = this.route.snapshot.paramMap.get('supplierId');      
        if(supplierId){  
          this.supplierId = parseInt(supplierId);
        }
        if(this.configuration) {
          this.configureRangeAmount();
          this.configureDateTimePicker();
          this.getNewDetails();
        }
      })
    );

    this.detailsSubscription = this.details$.subscribe(
      ((details: LoanWizardCalculatorDetails) => {
        if (details) {
          this.details = details;
        }
      })
    );

    this.paymentsQuantitySubscription = this.paymentsQuantity$.subscribe(
      ((paymentsQuantity: number) => {
        this.paymentsQuantity = paymentsQuantity;
      })
    );

    this.getDetailsSubjectSubscription = this.getDetailsSubject.pipe(
      debounceTime(250),
      tap(() => {
        this.store.dispatch(new LoanWizardCalculatorGetDetails())
      })
    ).subscribe();

    this.store.dispatch(new LoanWizardCalculatorGetConfiguration(this.supplierId));
  }

  private configureRangeAmount(): void {
    this.rangeAmount.min = this.configuration.minAmount;
    this.rangeAmount.max = this.configuration.maxAmount;
    this.rangeAmount.step = this.configuration.stepAmount;
    this.rangeAmount.value = this.configuration.minAmount;
  }

  private configureDateTimePicker(): void {
    const minDate = new Date();
    minDate.setDate(minDate.getDate() + this.configuration.minDays);
    const minDateString = this.getDateIso8601(minDate);
    this.dateTimePicker.nativeElement.min = minDateString;

    const maxDate = new Date();
    maxDate.setDate(maxDate.getDate() + this.configuration.maxDays);
    this.maxDateString = this.getDateIso8601(maxDate);
    this.dateTimePicker.nativeElement.max = this.maxDateString;

    this.minDateString = minDateString;
  }

  public rangeAmountChanged(): void {
    this.getNewDetails();
  }

  public validateDates(): boolean {
    let validationResult = true;
    const selectedDate = new Date(this.dateTimePicker.nativeElement.value);
    const minDate = new Date(this.minDateString);
    const maxDate = new Date(this.maxDateString);

    if (selectedDate <  minDate || selectedDate > maxDate) {
      validationResult = false;
      this.dateTimePicker.nativeElement.value = '';
      this.invalidDate = true;
    } else {
      this.invalidDate = false;
    }

    return validationResult;
  }

  public expirationDateChanged(): void {
    if(this.validateDates()) {
      this.getNewDetails();
    }
  }

  private calculateDaysDifference(): number {
    const expDate = new Date(Date.parse(this.dateTimePicker.nativeElement.value ? this.dateTimePicker.nativeElement.value : this.minDateString));

    let todayDate = new Date()
    todayDate = new Date(todayDate.getFullYear(),todayDate.getMonth() , todayDate.getDate());

    const diffTime = Math.abs(expDate.getTime() - todayDate.getTime());

    let diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

    this.daysQuantity = diffDays;

    return diffDays;
  }

  private getNewDetails(): void {
    const termInDays = this.calculateDaysDifference();
    this.store.dispatch(new LoanWizardCalculatorSetModel({amount: Number(this.rangeAmount.value.toString()), expirationDate: new Date(Date.parse(this.dateTimePicker.nativeElement.value)), paymentsQuantity: this.paymentsQuantity ? this.paymentsQuantity : 1, termInDays: termInDays}));
    this.getDetailsSubject.next();
  }


  private getDateIso8601(date: Date): string {
    return date.getFullYear().toString() + '-' + this.getNumberWithPaddingZero(date.getMonth() + 1) + '-' + this.getNumberWithPaddingZero(date.getDate())
  }

  private getNumberWithPaddingZero(num: number): string {
    if(num < 10) {
      return '0' + num.toString();
    } else {
      return num.toString();
    }
  }

  public async showDetails() {
    const modal = await this.modalController.create({
      component: ShowDetailsModalComponent
    });
    return await modal.present();
  }

  public async requestLoanClick() {
    let now = new Date();
    let nowInTimeZone = this.utils.changeTimezone(now, AppConfigService.settings.appInfo.timeZone);

    if (nowInTimeZone.getHours() < 6 || nowInTimeZone.getHours() >= 23) {
      const toast = await this.toastController.create({
        message: this.translate.instant('GLOBAL.OUT_OF_SERVICE'),
        position: 'bottom',
        duration: 10000
      });
      toast.present();
    } else {
      const token = this.store.selectSnapshot(AuthStateSelectors.getToken);
      if (token) {
        this.store.dispatch(new LoanWizardCalculatorSaveLoanConfiguration());
      } else {
        this.router.navigateByUrl('/have-account');
      }  
    }
  }

  ngOnDestroy(): void {
    this.configurationSubscription.unsubscribe();
    this.paymentsQuantitySubscription.unsubscribe();
    this.detailsSubscription.unsubscribe();
    this.getDetailsSubjectSubscription.unsubscribe();
  }
}
