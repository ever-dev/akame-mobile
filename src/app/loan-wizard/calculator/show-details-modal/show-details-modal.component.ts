import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';
import { LoanWizardCalculatorDetails } from 'src/app/store/loan-wizard/calculator/lw-calc-state.model';
import { LoanWizardCalculatorStateSelectors } from 'src/app/store/loan-wizard/calculator/lw-calc.selectors';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-loan-wizard-calculator-show-details-modal',
  templateUrl: './show-details-modal.component.html',
  styleUrls: ['./show-details-modal.component.scss'],
})
export class ShowDetailsModalComponent implements OnInit, OnDestroy {
  public details: LoanWizardCalculatorDetails;
  @Select(LoanWizardCalculatorStateSelectors.getDetails) details$: Observable<LoanWizardCalculatorDetails>;
  private detailsSubscription: Subscription;

  constructor(
    private store: Store
    , public modalController: ModalController
    , public router: Router
  ) {
  }

  ngOnInit(): void {
    this.detailsSubscription = this.details$.subscribe(
      ((details: LoanWizardCalculatorDetails) => {
        if (details) {
          this.details = details;
        }
      })
    );
  }

  public closeModal(): void {
    this.modalController.dismiss();
  }


  ngOnDestroy(): void {
    this.detailsSubscription.unsubscribe();
  }

  public learnMoreClick(): void {
    this.router.navigateByUrl('/documents/face-to-face');
  }
}
