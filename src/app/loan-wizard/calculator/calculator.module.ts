import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CalculatorComponent } from './calculator.component';
import { TranslateModule } from '@ngx-translate/core';
import { NgxsModule } from '@ngxs/store';
import { LoanWizardCalculatorState } from 'src/app/store/loan-wizard/calculator/lw-calc.state';
import { CalculatorService } from 'src/app/services/calculator.service';
import { ShowDetailsModalComponent } from './show-details-modal/show-details-modal.component';
import { CalculatorRoutingModule } from './calculator-routing.module';
import { CustomerService } from 'src/app/services/customer.service';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CalculatorRoutingModule,
        TranslateModule.forChild(),
        NgxsModule.forFeature([LoanWizardCalculatorState]),
    ],
    declarations: [
        CalculatorComponent,
        ShowDetailsModalComponent
    ],
    providers: [
        CalculatorService,
        CustomerService
    ],
    entryComponents: [
        ShowDetailsModalComponent
    ]
})
export class CalculatorModule { }
