import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfigService } from './config.service';
import { Observable } from 'rxjs';
import { LoanSettingsModel } from '../store/models/loan_settings.model';

@Injectable()
export class CustomerService {
  constructor(
    private http: HttpClient
  ) { }

  public getBanks(token: string): Observable<any> {
    return this.http.get(AppConfigService.settings.apiServer.baseUrl + '/backoffice/financial/bank/get', AppConfigService.getAuthHeaders(token));
  }

  public getBankAccountType(token: string): Observable<any> {
    return this.http.get(AppConfigService.settings.apiServer.baseUrl + '/backoffice/financial/bank_account_type/get', AppConfigService.getAuthHeaders(token));
  }

  public getEconomicIncomeType(token: string): Observable<any> {
    return this.http.get(AppConfigService.settings.apiServer.baseUrl + '/backoffice/economic_income_type/get', AppConfigService.getAuthHeaders(token));
  }

  public getTransportTypes(token: string): Observable<any> {
    return this.http.get(AppConfigService.settings.apiServer.baseUrl + '/backoffice/transport_type/get', AppConfigService.getAuthHeaders(token));
  }

  public getCompanyTypes(token: string): Observable<any> {
    return this.http.get(AppConfigService.settings.apiServer.baseUrl + '/backoffice/job/company_type/get', AppConfigService.getAuthHeaders(token));
  }

  public getContractTypes(token: string): Observable<any> {
    return this.http.get(AppConfigService.settings.apiServer.baseUrl + '/backoffice/job/contract_type/get', AppConfigService.getAuthHeaders(token));
  }

  public getFrequencyPayment(token: string): Observable<any> {
    return this.http.get(AppConfigService.settings.apiServer.baseUrl + '/backoffice/job/payment_frequency/get', AppConfigService.getAuthHeaders(token));
  }

  public getDepartaments(token: string): Observable<any> {
    return this.http.get(AppConfigService.settings.apiServer.baseUrl + '/backoffice/demography/departament/get', AppConfigService.getAuthHeaders(token));
  }

  public getCities(token: string, payload?: {
    departamentId?: number
  }): Observable<any> {
    return this.http.get(AppConfigService.settings.apiServer.baseUrl + '/backoffice/demography/city/get', AppConfigService.getAuthHeaders(token, payload));
  }

  public getPensioner(token: string): Observable<any> {
    return this.http.get(AppConfigService.settings.apiServer.baseUrl + '/backoffice/job/pension_fund/get', AppConfigService.getAuthHeaders(token));
  }

  public getIdentificationTypes(token: string): Observable<any> {
    return this.http.get(AppConfigService.settings.apiServer.baseUrl + '/backoffice/customer/identification_types/get', AppConfigService.getAuthHeaders(token));
  }

  public getCustomer(token: string): Observable<any> {
    return this.http.get(AppConfigService.settings.apiServer.baseUrl + '/frontoffice/customer/get', AppConfigService.getAuthHeaders(token));
  }

  public getCustomerJob(token: string): Observable<any> {
    return this.http.get(AppConfigService.settings.apiServer.baseUrl + '/frontoffice/job/get', AppConfigService.getAuthHeaders(token));
  }

  public updateCustomerJob(token: string, payload: {
    job: any
  }): Observable<any> {
    return this.http.put(AppConfigService.settings.apiServer.baseUrl + '/frontoffice/job/update', payload, AppConfigService.getAuthHeaders(token));
  }

  public saveLoanConfiguration(token: string, payload: {
    loan: LoanSettingsModel
  }): Observable<any> {
    return this.http.post(AppConfigService.settings.apiServer.baseUrl + '/frontoffice/customer/save_loan_configuration', payload, AppConfigService.getAuthHeaders(token));
  }

  public getBankCities(token: string, payload?: {
    departamentId?: number
  }): Observable<any> {
    return this.http.get(AppConfigService.settings.apiServer.baseUrl + '/backoffice/demography/get_bank_cities', AppConfigService.getAuthHeaders(token, payload));
  }

  public getActivesLoans(token: string): Observable<any> {
    return this.http.get(AppConfigService.settings.apiServer.baseUrl + '/backoffice/loan/get_loans_actives', AppConfigService.getAuthHeaders(token));
  }

  public getLoans(token: string): Observable<any> {
    return this.http.get(AppConfigService.settings.apiServer.baseUrl + '/backoffice/loan/get_loans', AppConfigService.getAuthHeaders(token));
  }

  public getSuppliers(token: string): Observable<any> {
    return this.http.get(AppConfigService.settings.apiServer.baseUrl + '/backoffice/suppliers/get', AppConfigService.getAuthHeaders(token));
  }  
}
