import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfigService } from './config.service';
import { Observable } from 'rxjs';

@Injectable()
export class CalculatorService {
  constructor(
    private http: HttpClient
  ) { }
 
  public getConfiguration(payload?: {
      supplierId?: number;
    }): Observable<any> {
    return this.http.get(AppConfigService.settings.apiServer.baseUrl + '/frontoffice/calculator/configuration', AppConfigService.getHeaders(payload));
  }

  public getDetails(payload: {
    amount: number;
    paymentsQuantity: number;
    termInDays: number;
  }): Observable<any> {  
    return this.http.get(AppConfigService.settings.apiServer.baseUrl + '/frontoffice/calculator/details', AppConfigService.getHeaders(payload));
  }
}
