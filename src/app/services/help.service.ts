import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfigService } from './config.service';
import { Observable } from 'rxjs';

@Injectable()
export class HelpService {
  constructor(
    private http: HttpClient
  ) { }

  public send_contact_form(payload: {
    contact: {
      fullName: string;
      email: string;
      message: string;
    }
  }): Observable<any> {
    console.log('<<<<<<<<<<', payload, AppConfigService.settings.apiServer.baseUrl);
    return this.http.post(AppConfigService.settings.apiServer.baseUrl + '/frontoffice/contact', payload, AppConfigService.getHeaders());
  }
}
