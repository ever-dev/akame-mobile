import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfigService } from './config.service';
import { Observable } from 'rxjs';

@Injectable()
export class DocumentService {
  constructor(
    private http: HttpClient
  ) { }

  public getDocument(token: string, payload?: {
    name?: string
    }): Observable<any> {
    return this.http.get(AppConfigService.settings.apiServer.baseUrl + '/backoffice/document/get', AppConfigService.getAuthHeaders(token, payload));
  }

  public getClientContract(token: string, payload?: {
    name?: string,
    is_render?: boolean
    }): Observable<any> {
    return this.http.get(AppConfigService.settings.apiServer.baseUrl + '/backoffice/document/get_client_contract', AppConfigService.getAuthHeaders(token, payload));
  }  
}
