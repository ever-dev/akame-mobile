import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { Globalization } from '@ionic-native/globalization/ngx';

@Injectable()
export class AppConfigService {
  static settings: IAppConfig;

   constructor(private http: HttpClient, private translate: TranslateService, private globalization: Globalization) {}
   load() {
    const jsonFile = `assets/config/config.json`;

    return new Promise<void>((resolve, reject) => {
        this.http.get(jsonFile).toPromise().then((response : IAppConfig) => {
           AppConfigService.settings = <IAppConfig>response;
           resolve();   //Return Sucess
        }).catch((response: any) => {
           reject(`Failed to load the config file`);
        });
    });
   }

   loadTranslations() {
    const availableLanguages = ['es', 'en'];
    this.translate.addLangs(availableLanguages)
    this.translate.setDefaultLang('es');

    return new Promise<void>((resolve, reject) => {
      this.globalization.getPreferredLanguage()
      .then(language => {
        const twoLetterLanguageCode = language.value.slice(0,2).toLowerCase();
        if (availableLanguages.includes(twoLetterLanguageCode)) {
          this.translate.use(twoLetterLanguageCode).toPromise().then(() => {
            resolve();
          }).catch(() => {
            reject();
          });
        }
      })
      .catch(_ => {
        this.translate.use('es').toPromise().then(() => {
          resolve();
        }).catch(() => {
          reject();
        });
      });
    });
   }

   static getQueryParams(queryParams: any): HttpParams {
    if (queryParams) {
      let params = new HttpParams();
      for (var key of Object.keys(queryParams)) {
        if (typeof(queryParams[key]) !== 'undefined') {
          params = params.append(key, queryParams[key]);
        }
      }
      return params;
    }
    return undefined;
   }

   static getHeaders(queryParams?: any): {headers: HttpHeaders, params: HttpParams} {
     let headers: HttpHeaders = new HttpHeaders();
     headers = headers.append('Content-Type', 'application/json');
     headers = headers.append('Accept', 'application/json');
     return { headers: headers, params: AppConfigService.getQueryParams(queryParams) };
   }

   static getAuthHeaders(token: string, queryParams?: any, responseType='json'): {headers: HttpHeaders, params: HttpParams, responseType: 'json'} {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.set('Authorization', 'Bearer ' + token);
    headers = headers.set('Content-Type', 'application/json');
    headers = headers.set('Accept', 'application/json');
    return { headers: headers, params: AppConfigService.getQueryParams(queryParams), responseType: responseType as 'json'};
   }
}

export interface IAppConfig {
  appInfo: {
    name: string,
    version: string,
    secondsUntilTokenAlert: number,
    timeZone: string
  }

  apiServer: {
    baseUrl: string,
    socketUrl: string
  }
}
