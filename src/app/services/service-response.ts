export class ServiceResponse<T> {
    data: T;
    details: {
        code: number;
        codeName: string;
        description: string;
        isError: boolean;
        exception: string;
    }
}
