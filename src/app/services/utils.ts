import { Injectable } from '@angular/core';
import { ToastController, LoadingController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class ServiceUtils {
  private isLoading = false;
  private loadingInstanceId: string = 'globalSpinner';
  constructor(
    private toastController: ToastController,
    private translate: TranslateService,
    public loadingController: LoadingController
  ) {
  }

  public async genericServiceErrorToast() {
    const toast = await this.toastController.create({
      message: this.translate.instant('GLOBAL.SERVICE_ERROR_DETAILS'),
      position: 'bottom',
      duration: 5000
    });
    toast.present();
  }

  public changeTimezone(date: Date, timeZoneString) {

    let invdate = new Date(date.toLocaleString('en-US', {
      timeZone: timeZoneString
    }));
  
    let diff = date.getTime() - invdate.getTime();
  
    return new Date(date.getTime() - diff); // needs to substract
  
  }
}
