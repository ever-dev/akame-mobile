import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngxs/store';
import { AuthStateSelectors } from '../store/auth/auth.selectors';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(
    private store: Store,
    private router: Router
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const token = this.store.selectSnapshot(AuthStateSelectors.getToken);
    const isRequestingLoginOrSignupPage = (route.url && route.url.length > 0 && (route.url[0].path === 'login' || route.url[0].path === 'signup' || route.url[0].path === 'have-account'));

    if (!token && !isRequestingLoginOrSignupPage) {
      this.router.navigate(['/']);
      return false;
    }

    if (token && isRequestingLoginOrSignupPage) {
      this.router.navigate(['/']);
    }

    return true;
  }
}
