import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfigService } from './config.service';
import { Observable } from 'rxjs';

@Injectable()
export class AuthService {
  constructor(
    private http: HttpClient
  ) { }

  public getToken(payload: {
    auth: {
      name: string;
      password: string;
      tfc: boolean;
    }
  }): Observable<any> {
    return this.http.post(AppConfigService.settings.apiServer.baseUrl + '/frontoffice/user/create_session', payload, AppConfigService.getHeaders());
  }

  public refreshToken(token: string): Observable<any> {
    return this.http.put(AppConfigService.settings.apiServer.baseUrl + '/frontoffice/user/refresh_session', null, AppConfigService.getAuthHeaders(token));
  }

  public signup(payload: {
    user: {
      name: string;
      password: string;
      email: string
    }
  }): Observable<any> {
    return this.http.post(AppConfigService.settings.apiServer.baseUrl + '/frontoffice/signup', payload, AppConfigService.getHeaders());
  }

  public confirmAccount(payload?: {
    token?: string;
  }): Observable<any> {
    return this.http.get(AppConfigService.settings.apiServer.baseUrl + '/frontoffice/confirm_account', AppConfigService.getHeaders(payload));
  }

  public sendForgotPasswordEmail(payload: {
    usernameOrEmail: string;
  }): Observable<any> {
    return this.http.post(AppConfigService.settings.apiServer.baseUrl + '/frontoffice/user/send_forgot_password_email', null, AppConfigService.getHeaders(payload));
  }

  public changePassword(token:string, payload: {
    password: string;
  }): Observable<any> {
    return this.http.put(AppConfigService.settings.apiServer.baseUrl + '/frontoffice/user/change_password', null, AppConfigService.getAuthHeaders(token, payload));
  }
}
