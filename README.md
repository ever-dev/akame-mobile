# Akame Mobile App

# Dev Dependencies
- Install node LTS: https://nodejs.org/en/download/
- Run this command:
    - npm install -g @ionic/cli cordova-res

# Run Application
- Run just in the browser
    - ionic serve
- Build Ionic
    - ionic build
- Run with Browser & Android & iOS previews
    - ionic serve --lab
- To Sync the project with Capacitor
    - npx cap sync
- To generate Android Studio Solution and Run App with an Emulator (with hotreload)
    - ionic cap run android -l --external

